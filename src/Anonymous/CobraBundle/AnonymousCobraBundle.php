<?php

namespace Anonymous\CobraBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AnonymousCobraBundle extends Bundle
{
    public function getParent() {
        return 'SonataUserBundle';
        }
}
