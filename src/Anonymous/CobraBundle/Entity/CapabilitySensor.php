<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="capabilitySensor")
  * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\CapabilitySensorRepository")

 */
class CapabilitySensor {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Sensor", inversedBy="capabilitySensor")
     * @ORM\JoinColumn(name="sensor_id", referencedColumnName="id")
     */
    private $sensor;

    /**
     * @ORM\ManyToOne(targetEntity="Capability", inversedBy="sensor")
     * @ORM\JoinColumn(name="capability_id", referencedColumnName="id")
     */
    private $capability;

    /**
     * Constructor
     */
    public function __construct() {
        $this->sensor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->capablility = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Get sensor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSensor() {
        return $this->sensor;
    }

    /**
     * Get capablility
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapablility() {
        return $this->capablility;
    }

    /**
     * Set sensor
     *
     * @param \Anonymous\CobraBundle\Entity\Sensor $sensor
     *
     * @return CapabilitySensor
     */
    public function setSensor(\Anonymous\CobraBundle\Entity\Sensor $sensor = null) {
        $this->sensor = $sensor;

        return $this;
    }

    /**
     * Set capablility
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capablility
     *
     * @return CapabilitySensor
     */
    public function setCapablility(\Anonymous\CobraBundle\Entity\Capability $capablility = null) {
        $this->capablility = $capablility;

        return $this;
    }


    /**
     * Set capability
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capability
     *
     * @return CapabilitySensor
     */
    public function setCapability(\Anonymous\CobraBundle\Entity\Capability $capability = null)
    {
        $this->capability = $capability;

        return $this;
    }

    /**
     * Get capability
     *
     * @return \Anonymous\CobraBundle\Entity\Capability
     */
    public function getCapability()
    {
        return $this->capability;
    }
}
