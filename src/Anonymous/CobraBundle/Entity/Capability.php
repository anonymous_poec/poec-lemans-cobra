<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="capability")
 * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\CapabilityRepository")
 */
class Capability {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="CapabilityActor", mappedBy="capability",cascade={"remove"})
     */
    private $actor;

    /**
     * @ORM\OneToMany(targetEntity="CapabilitySensor", mappedBy="capability",cascade={"remove"})
     */
    private $sensor;

    /**
     * @ORM\ManyToOne(targetEntity="Node", inversedBy="capabilities")
     * @ORM\JoinColumn(name="node_id", referencedColumnName="id")     
     */
    private $node;

    /**
     * Constructor
     */
    public function __construct() {
        $this->actor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sensor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Capability
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Add actor
     *
     * @param \Anonymous\CobraBundle\Entity\Actor $actor
     *
     * @return Capability
     */
    public function addActor(\Anonymous\CobraBundle\Entity\Actor $actor) {
        $this->actor[] = $actor;

        return $this;
    }

    /**
     * Remove actor
     *
     * @param \Anonymous\CobraBundle\Entity\Actor $actor
     */
    public function removeActor(\Anonymous\CobraBundle\Entity\Actor $actor) {
        $this->actor->removeElement($actor);
    }

    /**
     * Get actor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActor() {
        return $this->actor;
    }

    /**
     * Add sensor
     *
     * @param \Anonymous\CobraBundle\Entity\Sensor $sensor
     *
     * @return Capability
     */
    public function addSensor(\Anonymous\CobraBundle\Entity\Sensor $sensor) {
        $this->sensor[] = $sensor;

        return $this;
    }

    /**
     * Remove sensor
     *
     * @param \Anonymous\CobraBundle\Entity\Sensor $sensor
     */
    public function removeSensor(\Anonymous\CobraBundle\Entity\Sensor $sensor) {
        $this->sensor->removeElement($sensor);
    }

    /**
     * Get sensor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSensor() {
        return $this->sensor;
    }


    /**
     * Set node
     *
     * @param \Anonymous\CobraBundle\Entity\Node $node
     *
     * @return Capability
     */
    public function setNode(\Anonymous\CobraBundle\Entity\Node $node = null)
    {
        $this->node = $node;

        return $this;
    }

    /**
     * Get node
     *
     * @return \Anonymous\CobraBundle\Entity\Node
     */
    public function getNode()
    {
        return $this->node;
    }
}
