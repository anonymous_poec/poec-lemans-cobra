<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sensor")
  * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\SensorRepository")
 */
class Sensor {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;
    /**
     *@ORM\Column(type="string", nullable=false)
     */
    private $name;
    
        /**
     *@ORM\OneToMany(targetEntity="CapabilitySensor", mappedBy="sensor",cascade={"remove"})
     */
    private  $capabilitySensor;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sensor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add capabitity
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capabitity
     *
     * @return Sensor
     */
    public function addCapabitity(\Anonymous\CobraBundle\Entity\Capability $capabitity)
    {
        $this->capabitities[] = $capabitity;

        return $this;
    }

    /**
     * Remove capabitity
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capabitity
     */
    public function removeCapabitity(\Anonymous\CobraBundle\Entity\Capability $capabitity)
    {
        $this->capabitities->removeElement($capabitity);
    }

    /**
     * Get capabitities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapabitities()
    {
        return $this->capabitities;
    }

    /**
     * Add capabititySensor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilitySensor $capabititySensor
     *
     * @return Sensor
     */
    public function addCapabititySensor(\Anonymous\CobraBundle\Entity\CapabilitySensor $capabititySensor)
    {
        $this->capabititySensor[] = $capabititySensor;

        return $this;
    }

    /**
     * Remove capabititySensor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilitySensor $capabititySensor
     */
    public function removeCapabititySensor(\Anonymous\CobraBundle\Entity\CapabilitySensor $capabititySensor)
    {
        $this->capabititySensor->removeElement($capabititySensor);
    }

    /**
     * Get capabititySensor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapabititySensor()
    {
        return $this->capabititySensor;
    }

    /**
     * Add capabilitySensor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilitySensor $capabilitySensor
     *
     * @return Sensor
     */
    public function addCapabilitySensor(\Anonymous\CobraBundle\Entity\CapabilitySensor $capabilitySensor)
    {
        $this->capabilitySensor[] = $capabilitySensor;

        return $this;
    }

    /**
     * Remove capabilitySensor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilitySensor $capabilitySensor
     */
    public function removeCapabilitySensor(\Anonymous\CobraBundle\Entity\CapabilitySensor $capabilitySensor)
    {
        $this->capabilitySensor->removeElement($capabilitySensor);
    }

    /**
     * Get capabilitySensor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapabilitySensor()
    {
        return $this->capabilitySensor;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->capabilitySensor = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
