<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="capabilityActor")
 * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\CapabilityActorRepository")

 */
class CapabilityActor {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Actor", inversedBy="capabilityActor")
     * @ORM\JoinColumn(name="actor_id", referencedColumnName="id")     
     */
    private $actor;

    /**
     * @ORM\ManyToOne(targetEntity="Capability", inversedBy="actor")
     * @ORM\JoinColumn(name="capability_id", referencedColumnName="id")
     */
    private $capability;

    /**
     * Constructor
     */
    public function __construct() {
        $this->actor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get actor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActor() {
        return $this->actor;
    }

    /**
     * Set actor
     *
     * @param \Anonymous\CobraBundle\Entity\Actor $actor
     *
     * @return CapabilityActor
     */
    public function setActor(\Anonymous\CobraBundle\Entity\Actor $actor = null) {
        $this->actor = $actor;

        return $this;
    }

    /**
     * Set capability
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capability
     *
     * @return CapabilityActor
     */
    public function setCapability(\Anonymous\CobraBundle\Entity\Capability $capability = null) {
        $this->capability = $capability;

        return $this;
    }

    /**
     * Get capability
     *
     * @return \Anonymous\CobraBundle\Entity\Capability
     */
    public function getCapability() {
        return $this->capability;
    }

}
