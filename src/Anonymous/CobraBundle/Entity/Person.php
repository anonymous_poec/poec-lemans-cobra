<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use JMS\Serializer\Annotation as JSON;

/**
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\PersonRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Person extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     *
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="date",nullable=true) 
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     */
    protected $birth;

    /**
     * @ORM\OneToMany(targetEntity="Right" , mappedBy="person",cascade={"remove"})
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     */
    protected $right;

    /**
     * @ORM\OneToMany(targetEntity="PlaceUser" , mappedBy="user")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")
     */
    protected $userPlace;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->right = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /** @ORM\PrePersist */
    public function setCreatedAtValue() {
        $this->setRoles(array('ROLE_USER'));
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     *
     * @return Person
     */
    public function setBirth($birth) {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth() {
        return $this->birth;
    }

    /**
     * Add right
     *
     * @param \Anonymous\CobraBundle\Entity\Right $right
     *
     * @return Person
     */
    public function addRight(\Anonymous\CobraBundle\Entity\Right $right) {
        $this->right[] = $right;

        return $this;
    }

    /**
     * Remove right
     *
     * @param \Anonymous\CobraBundle\Entity\Right $right
     */
    public function removeRight(\Anonymous\CobraBundle\Entity\Right $right) {
        $this->right->removeElement($right);
    }

    /**
     * Get right
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRight() {
        return $this->right;
    }

    /**
     * Add userPlace
     *
     * @param \Anonymous\CobraBundle\Entity\PlaceUser $userPlace
     *
     * @return Person
     */
    public function addUserPlace(\Anonymous\CobraBundle\Entity\PlaceUser $userPlace) {
        $this->userPlace[] = $userPlace;

        return $this;
    }

    /**
     * Remove userPlace
     *
     * @param \Anonymous\CobraBundle\Entity\PlaceUser $userPlace
     */
    public function removeUserPlace(\Anonymous\CobraBundle\Entity\PlaceUser $userPlace) {
        $this->userPlace->removeElement($userPlace);
    }

    /**
     * Get userPlace
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPlace() {
        return $this->userPlace;
    }

}
