<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="node")
  * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\NodeRepository")
 */
class Node {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Capability", mappedBy="node",cascade={"remove"})
     */
    private $capabilities;

    /**
     * @ORM\Column(type="string")
     */
    private $ipAdress;

    /**
     * @ORM\Column(type="string")
     */
    private $security;
    
    /**
     * @ORM\ManyToOne(targetEntity="Zone", inversedBy="node")
     * @ORM\JoinColumn(name="zone_id", referencedColumnName="id") 
     */
    private $zone;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->capabilities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Node
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ipAdress
     *
     * @param string $ipAdress
     *
     * @return Node
     */
    public function setIpAdress($ipAdress)
    {
        $this->ipAdress = $ipAdress;

        return $this;
    }

    /**
     * Get ipAdress
     *
     * @return string
     */
    public function getIpAdress()
    {
        return $this->ipAdress;
    }

    /**
     * Set security
     *
     * @param string $security
     *
     * @return Node
     */
    public function setSecurity($security)
    {
        $this->security = $security;

        return $this;
    }

    /**
     * Get security
     *
     * @return string
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * Add capability
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capability
     *
     * @return Node
     */
    public function addCapability(\Anonymous\CobraBundle\Entity\Capability $capability)
    {
        $this->capabilities[] = $capability;

        return $this;
    }

    /**
     * Remove capability
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capability
     */
    public function removeCapability(\Anonymous\CobraBundle\Entity\Capability $capability)
    {
        $this->capabilities->removeElement($capability);
    }

    /**
     * Get capabilities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapabilities()
    {
        return $this->capabilities;
    }

    /**
     * Set zone
     *
     * @param \Anonymous\CobraBundle\Entity\Zone $zone
     *
     * @return Node
     */
    public function setZone(\Anonymous\CobraBundle\Entity\Zone $zone = null)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return \Anonymous\CobraBundle\Entity\Zone
     */
    public function getZone()
    {
        return $this->zone;
    }
}
