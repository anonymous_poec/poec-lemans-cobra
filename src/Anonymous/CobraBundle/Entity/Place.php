<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JSON;

/**
 * @ORM\Entity
 * @ORM\Table(name="place")
 * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\PlaceRepository")
 * @JSON\ExclusionPolicy("ALL")
 */
class Place {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     *
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     */
    private $adress;

    /**
     * @ORM\ManyToOne(targetEntity="City" )
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable= false)
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     */
    private $city;

    /**
     * @ORM\Column(type="boolean")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="Zone", mappedBy="place",cascade={"remove"})
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     */
    private $zone;

    /**
     * @ORM\OneToMany(targetEntity="PlaceUser", mappedBy="place")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     */
    private $placeUser;

    /**
     * Constructor
     */
    public function __construct() {
        $this->city = new \Doctrine\Common\Collections\ArrayCollection();
        $this->zone = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Place
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set adress
     *
     * @param string $adress
     *
     * @return Place
     */
    public function setAdress($adress) {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress() {
        return $this->adress;
    }

    /**
     * Set owner
     *
     * @param boolean $owner
     *
     * @return Place
     */
    public function setOwner($owner) {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return boolean
     */
    public function getOwner() {
        return $this->owner;
    }

    /**
     * Add city
     *
     * @param \Anonymous\CobraBundle\Entity\City $city
     *
     * @return Place
     */
    public function addCity(\Anonymous\CobraBundle\Entity\City $city) {
        $this->city[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param \Anonymous\CobraBundle\Entity\City $city
     */
    public function removeCity(\Anonymous\CobraBundle\Entity\City $city) {
        $this->city->removeElement($city);
    }

    /**
     * Get city
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Add zone
     *
     * @param \Anonymous\CobraBundle\Entity\Zone $zone
     *
     * @return Place
     */
    public function addZone(\Anonymous\CobraBundle\Entity\Zone $zone) {
        $this->zone[] = $zone;
        if ($zone->getPlace() != $this) {
            $zone->setPlace($this);
        }
        return $this;
    }

    /**
     * Remove zone
     *
     * @param \Anonymous\CobraBundle\Entity\Zone $zone
     */
    public function removeZone(\Anonymous\CobraBundle\Entity\Zone $zone) {
        $this->zone->removeElement($zone);
        if ($zone->getPlace() == $this) {
            $zone->setPlace(null);
        }
    }

    /**
     * Get zone
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZone() {
        return $this->zone;
    }

    /**
     * Set person
     *
     * @param \Anonymous\CobraBundle\Entity\Person $person
     *
     * @return Place
     */
    public function setPerson(\Anonymous\CobraBundle\Entity\Person $person = null) {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Anonymous\CobraBundle\Entity\Person
     */
    public function getPerson() {
        return $this->person;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Place
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }


    /**
     * Add place
     *
     * @param \Anonymous\CobraBundle\Entity\PlaceUser $place
     *
     * @return Place
     */
    public function addPlace(\Anonymous\CobraBundle\Entity\PlaceUser $place)
    {
        $this->place[] = $place;

        return $this;
    }

    /**
     * Remove place
     *
     * @param \Anonymous\CobraBundle\Entity\PlaceUser $place
     */
    public function removePlace(\Anonymous\CobraBundle\Entity\PlaceUser $place)
    {
        $this->place->removeElement($place);
    }

    /**
     * Get place
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Add placeUser
     *
     * @param \Anonymous\CobraBundle\Entity\PlaceUser $placeUser
     *
     * @return Place
     */
    public function addPlaceUser(\Anonymous\CobraBundle\Entity\PlaceUser $placeUser)
    {
        $this->placeUser[] = $placeUser;

        return $this;
    }

    /**
     * Remove placeUser
     *
     * @param \Anonymous\CobraBundle\Entity\PlaceUser $placeUser
     */
    public function removePlaceUser(\Anonymous\CobraBundle\Entity\PlaceUser $placeUser)
    {
        $this->placeUser->removeElement($placeUser);
    }

    /**
     * Get placeUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlaceUser()
    {
        return $this->placeUser;
    }
}
