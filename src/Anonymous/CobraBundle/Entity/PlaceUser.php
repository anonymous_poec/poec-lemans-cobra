<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JSON;

/**
 * @ORM\Table(name="placeUser")
 * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\PlaceRepository")
 * @JSON\ExclusionPolicy("ALL")
 */
class PlaceUser {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JSON\Expose
     * @JSON\Groups({"api_process"})
     * @JSON\Since("1.0")

     *
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="userPlace")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @JSON\Expose
     * @var type 
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="placeUser")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id")
     * @JSON\Expose
     * @var type 
     */
    private $place;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Anonymous\CobraBundle\Entity\Person $user
     *
     * @return PlaceUser
     */
    public function setUser(\Anonymous\CobraBundle\Entity\Person $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Anonymous\CobraBundle\Entity\Person
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set place
     *
     * @param \Anonymous\CobraBundle\Entity\Place $place
     *
     * @return PlaceUser
     */
    public function setPlace(\Anonymous\CobraBundle\Entity\Place $place = null) {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \Anonymous\CobraBundle\Entity\Place
     */
    public function getPlace() {
        return $this->place;
    }

}
