<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="zone")
  * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\ZoneRepository")
 */
class Zone {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="zone")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id")
     */
    private $place;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInDoor;

    /**
     * @ORM\OneToMany(targetEntity="Node", mappedBy="zone", cascade={"remove"})
     */
    private $node;

    /**
     * Constructor
     */
    public function __construct() {
        $this->node = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Zone
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set isInDoor
     *
     * @param boolean $isInDoor
     *
     * @return Zone
     */
    public function setIsInDoor($isInDoor) {
        $this->isInDoor = $isInDoor;

        return $this;
    }

    /**
     * Get isInDoor
     *
     * @return boolean
     */
    public function getIsInDoor() {
        return $this->isInDoor;
    }

    /**
     * Set place
     *
     * @param \Anonymous\CobraBundle\Entity\Place $place
     *
     * @return Zone
     */
    public function setPlace(\Anonymous\CobraBundle\Entity\Place $place = null) {
        $this->place = $place;
           if (!$place->getZone()->contains($this))
        {
            $place->addZone($this);
        }
        return $this;
    }

    /**
     * Get place
     *
     * @return \Anonymous\CobraBundle\Entity\Place
     */
    public function getPlace() {
        return $this->place;
    }

    /**
     * Add node
     *
     * @param \Anonymous\CobraBundle\Entity\Node $node
     *
     * @return Zone
     */
    public function addNode(\Anonymous\CobraBundle\Entity\Node $node) {
        $this->node[] = $node;
         if ($node->getZone() != $this)
        {
            $node->setZone($this);
        }

        return $this;
    }

    /**
     * Remove node
     *
     * @param \Anonymous\CobraBundle\Entity\Node $node
     */
    public function removeNode(\Anonymous\CobraBundle\Entity\Node $node) {
        $this->node->removeElement($node);
          
    }

    /**
     * Get node
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNode() {
        return $this->node;
    }

}
