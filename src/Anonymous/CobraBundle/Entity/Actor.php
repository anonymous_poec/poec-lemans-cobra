<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Anonymous\CobraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="actor")
 * @ORM\Entity(repositoryClass="Anonymous\CobraBundle\Repository\ActorRepository")
 */
class Actor {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     */
    private $id;
    /**
     *@ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     *@ORM\OneToMany(targetEntity="CapabilityActor", mappedBy="actor",cascade={"remove"})
     */
    private  $capabilityActor;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->capabitities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Actor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add capabitity
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capabitity
     *
     * @return Actor
     */
    public function addCapabitity(\Anonymous\CobraBundle\Entity\Capability $capabitity)
    {
        $this->capabitities[] = $capabitity;

        return $this;
    }

    /**
     * Remove capabitity
     *
     * @param \Anonymous\CobraBundle\Entity\Capability $capabitity
     */
    public function removeCapabitity(\Anonymous\CobraBundle\Entity\Capability $capabitity)
    {
        $this->capabitities->removeElement($capabitity);
    }

    /**
     * Get capabitities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapabitities()
    {
        return $this->capabitities;
    }

    /**
     * Add capabitityActor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilityActor $capabitityActor
     *
     * @return Actor
     */
    public function addCapabitityActor(\Anonymous\CobraBundle\Entity\CapabilityActor $capabitityActor)
    {
        $this->capabitityActor[] = $capabitityActor;

        return $this;
    }

    /**
     * Remove capabitityActor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilityActor $capabitityActor
     */
    public function removeCapabitityActor(\Anonymous\CobraBundle\Entity\CapabilityActor $capabitityActor)
    {
        $this->capabitityActor->removeElement($capabitityActor);
    }

    /**
     * Get capabitityActor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapabitityActor()
    {
        return $this->capabitityActor;
    }

    /**
     * Add capabilityActor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilityActor $capabilityActor
     *
     * @return Actor
     */
    public function addCapabilityActor(\Anonymous\CobraBundle\Entity\CapabilityActor $capabilityActor)
    {
        $this->capabilityActor[] = $capabilityActor;

        return $this;
    }

    /**
     * Remove capabilityActor
     *
     * @param \Anonymous\CobraBundle\Entity\CapabilityActor $capabilityActor
     */
    public function removeCapabilityActor(\Anonymous\CobraBundle\Entity\CapabilityActor $capabilityActor)
    {
        $this->capabilityActor->removeElement($capabilityActor);
    }

    /**
     * Get capabilityActor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapabilityActor()
    {
        return $this->capabilityActor;
    }
}
