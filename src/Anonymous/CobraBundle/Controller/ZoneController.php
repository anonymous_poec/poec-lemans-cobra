<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Anonymous\CobraBundle\Entity\Zone;
use Anonymous\CobraBundle\Form\ZoneType;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Zone controller.
 *
 * @Route("/place/{idPlace}/zone")
 */
class ZoneController extends FOSRestController {

    /**
     * Lists all Zone entities.
     *
     * @Route("/", name="zone")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($idPlace) {
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->createQuery(
            'SELECT z
             FROM AnonymousCobraBundle:Zone z
             WHERE z.place = :idPlace
          '
            )->setParameter('idPlace',$idPlace);

            $entities = $query->getResult();
        
            
             $nbNodes = array();
             foreach($entities as $node)
                {

                  $nbNodes[$node->getId()] = $node->getNode($em->getRepository('AnonymousCobraBundle:Node')->countnode($node->getId()));
                 
                }
        

        
        $deleteForms = array();
        Foreach($entities as $zone)
        {
           
        $deleteForms[$zone->getId()] = $this->createDeleteForm($zone->getId(),$idPlace)->createView();
        
        }
        
        return array(
            'entities' => $entities,
            'idPlace' => $idPlace,
            'deleteForms'=> $deleteForms,
            'result'=> $nbNodes,
        );
    }

    /**
     * Creates a new Zone entity.
     *
     * @Route("/", name="zone_create")
     * @Method("POST")
     * @Template("AnonymousCobraBundle:Zone:new.html.twig")
     */
    public function createAction(Request $request, $idPlace) {
        $entity = new Zone();
        $form = $this->createCreateForm($entity, $idPlace);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $place = $em->getRepository('AnonymousCobraBundle:Place')->find($idPlace);
            $entity->setPlace($place);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('zone_show', array(
                'id' => $entity->getId(), 
                'idPlace' => $idPlace)));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'idPlace' => $idPlace
        );
    }

    /**
     * Creates a form to create a Zone entity.
     *
     * @param Zone $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Zone $entity, $idPlace) {
        $form = $this->createForm(new ZoneType(), $entity, array(
            'action' => $this->generateUrl('zone_create', array('idPlace' => $idPlace)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Zone entity.
     *
     * @Route("/new", name="zone_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($idPlace) {
        $entity = new Zone();
        $form = $this->createCreateForm($entity, $idPlace);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'idPlace' => $idPlace
        );
    }

    /**
     * Finds and displays a Zone entity.
     *
     * @Route("/{id}", name="zone_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id,$idPlace) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Zone')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Zone entity.');
        }

        $deleteForm = $this->createDeleteForm($id,$idPlace);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'idPlace'=> $idPlace
        );
    }

    /**
     * Displays a form to edit an existing Zone entity.
     *
     * @Route("/{id}/edit", name="zone_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id,$idPlace) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Zone')->find($id);
//$entity=new Zone();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Zone entity.');
        }

        $editForm = $this->createEditForm($entity,$idPlace);
        $deleteForm = $this->createDeleteForm($id, $idPlace);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'idPlace'=>$idPlace
        );
    }

    /**
     * Creates a form to edit a Zone entity.
     *
     * @param Zone $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Zone $entity,$idPlace) {
        $form = $this->createForm(new ZoneType(), $entity, array(
            'action' => $this->generateUrl('zone_update', array(
                'id' => $entity->getId(),
                'idPlace'=> $idPlace)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Zone entity.
     *
     * @Route("/{id}", name="zone_update")
     * @Method("PUT")
     * @Template("AnonymousCobraBundle:Zone:edit.html.twig")
     */
    public function updateAction(Request $request, $id,$idPlace) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Zone')->find($id);
        //$entity = new Zone();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Zone entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $entity->getPlace()->getId());
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('zone_edit', array('id' => $id,'idPlace'=>$idPlace)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'idPlace'=>$idPlace
        );
    }

    /**
     * Deletes a Zone entity.
     *
     * @Route("/{id}", name="zone_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id,$idPlace) {
        $form = $this->createDeleteForm($id,$idPlace);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnonymousCobraBundle:Zone')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Zone entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('zone',array('id' => $id, 'idPlace' => $idPlace)));
    }

    /**
     * Creates a form to delete a Zone entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $idPlace) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('zone_delete', array('id' => $id, 'idPlace' => $idPlace)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete',
                            'attr' => array(
                            'onclick' => 'return confirm("Are you sure?")'
                        )))
                        ->getForm()
        ;
    }

}
