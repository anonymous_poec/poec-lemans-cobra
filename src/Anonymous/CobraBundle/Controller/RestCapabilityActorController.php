<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Controller;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class RestCapabilityActorController extends Controller {
    /**
     * @ApiDoc()
     * @return type
     */
     public function getCapabilityActorAction()
    {
         $em=  $this->getDoctrine() -> getManager();
        $data = $em ->getRepository('AnonymousCobraBundle:CapabilityActor')->findAll(); // get data, in this case list of users.
        $view = $this->view($data, 200)
            ->setTemplate("MyBundle:Users:getUsers.html.twig")
            ->setTemplateVar('capabilityActor')
        ;

        return $this->handleView($view);
    }
}