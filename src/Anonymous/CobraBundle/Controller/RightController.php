<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Anonymous\CobraBundle\Entity\Right;
use Anonymous\CobraBundle\Form\RightType;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Right controller.
 *
 * @Route("/right")
 */
class RightController extends FOSRestController
{

    /**
     * Lists all Right entities.
     *
     * @Route("/", name="right")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AnonymousCobraBundle:Right')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Right entity.
     *
     * @Route("/", name="right_create")
     * @Method("POST")
     * @Template("AnonymousCobraBundle:Right:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Right();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('right_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Right entity.
     *
     * @param Right $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Right $entity)
    {
        $form = $this->createForm(new RightType(), $entity, array(
            'action' => $this->generateUrl('right_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Right entity.
     *
     * @Route("/new", name="right_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Right();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Right entity.
     *
     * @Route("/{id}", name="right_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Right')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Right entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Right entity.
     *
     * @Route("/{id}/edit", name="right_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Right')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Right entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Right entity.
    *
    * @param Right $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Right $entity)
    {
        $form = $this->createForm(new RightType(), $entity, array(
            'action' => $this->generateUrl('right_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Right entity.
     *
     * @Route("/{id}", name="right_update")
     * @Method("PUT")
     * @Template("AnonymousCobraBundle:Right:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Right')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Right entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('right_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Right entity.
     *
     * @Route("/{id}", name="right_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnonymousCobraBundle:Right')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Right entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('right'));
    }

    /**
     * Creates a form to delete a Right entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('right_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
