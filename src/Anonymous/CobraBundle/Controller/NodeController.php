<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Anonymous\CobraBundle\Entity\Node;
use Anonymous\CobraBundle\Form\NodeType;
use FOS\RestBundle\Controller\FOSRestController;


/**
 * Node controller.
 *
 * @Route("/place/{idPlace}/zone/{idZone}/node")
 */
class NodeController extends FOSRestController {

    /**
     * Lists all Node entities.
     *
     * @Route("/", name="node")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($idPlace, $idZone) {
        $em = $this->getDoctrine()->getManager();
        
        $query = $em->createQuery(
            'SELECT n
             FROM AnonymousCobraBundle:Node n
             WHERE n.zone = :idZone
          '
            )->setParameter('idZone',$idZone);

            $entities = $query->getResult();
        
             $deleteForms = array();
        Foreach($entities as $node)
        {
           
        $deleteForms[$node->getId()] = $this->createDeleteForm($node->getId(),$idPlace,$idZone)->createView();
        
        }
            
        
      //  $entities = $em->getRepository('AnonymousCobraBundle:Node')->findAll($idPlace, $idZone);

        return array(
            'entities' => $entities,
            'idPlace' => $idPlace,
            'idZone' => $idZone,
            'deleteForms'=> $deleteForms,
        );
    }

    /**
     * Creates a new Node entity.
     *
     * @Route("/", name="node_create")
     * @Method("POST")
     * @Template("AnonymousCobraBundle:Node:new.html.twig")
     */
    public function createAction(Request $request, $idPlace, $idZone) {
        $entity = new Node();
        $form = $this->createCreateForm($entity, $idPlace, $idZone);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $zone = $em->getRepository('AnonymousCobraBundle:Zone')->find($idZone);
            $entity->setZone($zone);
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('node_show', array('id' => $entity->getId(), 'idPlace' => $idPlace, 'idZone' => $idZone)));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'idPlace' => $idPlace,
            'idZone' => $idZone
        );
    }

    /**
     * Creates a form to create a Node entity.
     *
     * @param Node $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Node $entity, $idPlace, $idZone) {
        $form = $this->createForm(new NodeType(), $entity, array(
            'action' => $this->generateUrl('node_create', array('idPlace' => $idPlace, 'idZone' => $idZone)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Node entity.
     *
     * @Route("/new", name="node_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($idPlace, $idZone) {
        $entity = new Node();
        $form = $this->createCreateForm($entity, $idPlace, $idZone);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'idPlace' => $idPlace,
            'idZone' => $idZone
        );
    }

    /**
     * Finds and displays a Node entity.
     *
     * @Route("/{id}", name="node_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $idPlace, $idZone) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Node')->find($id);
//$entity=new Node();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Node entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $idPlace, $idZone);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'idPlace' => $idPlace,
            'idZone' => $idZone
        );
    }

    /**
     * Displays a form to edit an existing Node entity.
     *
     * @Route("/{id}/edit", name="node_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $idPlace) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Node')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Node entity.');
        }

        $editForm = $this->createEditForm($entity, $idPlace);
        $deleteForm = $this->createDeleteForm($id, $idPlace, $entity->getZone()->getId());

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'idPlace' => $idPlace
        );
    }

    /**
     * Creates a form to edit a Node entity.
     *
     * @param Node $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Node $entity, $idPlace) {
        $form = $this->createForm(new NodeType(), $entity, array(
            'action' => $this->generateUrl('node_update', array(
                'id' => $entity->getId(),
                'idZone' => $entity->getZone()->getId(),
                'idPlace' => $idPlace)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Node entity.
     *
     * @Route("/{id}", name="node_update")
     * @Method("PUT")
     * @Template("AnonymousCobraBundle:Node:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $idPlace) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Node')->find($id);
//$entity=new Node();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Node entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $idPlace, $entity->getZone()->getId());
        $editForm = $this->createEditForm($entity, $idPlace);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('node_edit', array('id' => $id, 'idZone' => $entity->getZone()->getId(), 'idPlace' => $idPlace)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'idPlace' => $idPlace
        );
    }

    /**
     * Deletes a Node entity.
     *
     * @Route("/{id}", name="node_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $idPlace, $idZone) {
        $form = $this->createDeleteForm($id, $idPlace, $idZone);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnonymousCobraBundle:Node')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Node entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('node', array('idPlace' => $idPlace, 'idZone' => $idZone)));
    }

    /**
     * Creates a form to delete a Node entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $idPlace, $idZone) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('node_delete', array('id' => $id, 'idPlace' => $idPlace, 'idZone' => $idZone)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->add('submit', 'submit', array('label' => 'Delete',
                            'attr' => array(
                            'onclick' => 'return confirm("Are you sure?")'
                        )))
                        ->getForm()
        ;
    }

}
