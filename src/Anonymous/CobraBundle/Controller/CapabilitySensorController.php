<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Anonymous\CobraBundle\Entity\CapabilitySensor;
use Anonymous\CobraBundle\Form\CapabilitySensorType;
use FOS\RestBundle\Controller\FOSRestController;


/**
 * CapabilitySensor controller.
 *
 * @Route("/capabilitysensor")
 */
class CapabilitySensorController extends FOSRestController
{

    /**
     * Lists all CapabilitySensor entities.
     *
     * @Route("/", name="capabilitysensor")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AnonymousCobraBundle:CapabilitySensor')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new CapabilitySensor entity.
     *
     * @Route("/", name="capabilitysensor_create")
     * @Method("POST")
     * @Template("AnonymousCobraBundle:CapabilitySensor:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new CapabilitySensor();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('capabilitysensor_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a CapabilitySensor entity.
     *
     * @param CapabilitySensor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CapabilitySensor $entity)
    {
        $form = $this->createForm(new CapabilitySensorType(), $entity, array(
            'action' => $this->generateUrl('capabilitysensor_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CapabilitySensor entity.
     *
     * @Route("/new", name="capabilitysensor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new CapabilitySensor();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a CapabilitySensor entity.
     *
     * @Route("/{id}", name="capabilitysensor_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:CapabilitySensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CapabilitySensor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CapabilitySensor entity.
     *
     * @Route("/{id}/edit", name="capabilitysensor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:CapabilitySensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CapabilitySensor entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CapabilitySensor entity.
    *
    * @param CapabilitySensor $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CapabilitySensor $entity)
    {
        $form = $this->createForm(new CapabilitySensorType(), $entity, array(
            'action' => $this->generateUrl('capabilitysensor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CapabilitySensor entity.
     *
     * @Route("/{id}", name="capabilitysensor_update")
     * @Method("PUT")
     * @Template("AnonymousCobraBundle:CapabilitySensor:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:CapabilitySensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CapabilitySensor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('capabilitysensor_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a CapabilitySensor entity.
     *
     * @Route("/{id}", name="capabilitysensor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnonymousCobraBundle:CapabilitySensor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CapabilitySensor entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('capabilitysensor'));
    }

    /**
     * Creates a form to delete a CapabilitySensor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('capabilitysensor_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
