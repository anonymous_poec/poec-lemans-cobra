<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Controller;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class RestZoneController extends Controller {
    /**
     * @ApiDoc()
     * @return type
     */
     public function getZoneAction()
    {
         $em=  $this->getDoctrine() -> getManager();
        $data = $em ->getRepository('AnonymousCobraBundle:Zone')->findAll(); // get data, in this case list of users.
        $view = $this->view($data, 200)
            ->setTemplate("MyBundle:Users:getZones.html.twig")
            ->setTemplateVar('zone')
        ;

        return $this->handleView($view);
    }
}