<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Anonymous\CobraBundle\Entity\CapabilityActor;
use Anonymous\CobraBundle\Form\CapabilityActorType;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * CapabilityActor controller.
 *
 * @Route("/capabilityactor")
 */
class CapabilityActorController extends FOSRestController
{

    /**
     * Lists all CapabilityActor entities.
     *
     * @Route("/", name="capabilityactor")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AnonymousCobraBundle:CapabilityActor')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new CapabilityActor entity.
     *
     * @Route("/", name="capabilityactor_create")
     * @Method("POST")
     * @Template("AnonymousCobraBundle:CapabilityActor:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new CapabilityActor();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('capabilityactor_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a CapabilityActor entity.
     *
     * @param CapabilityActor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CapabilityActor $entity)
    {
        $form = $this->createForm(new CapabilityActorType(), $entity, array(
            'action' => $this->generateUrl('capabilityactor_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CapabilityActor entity.
     *
     * @Route("/new", name="capabilityactor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new CapabilityActor();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a CapabilityActor entity.
     *
     * @Route("/{id}", name="capabilityactor_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:CapabilityActor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CapabilityActor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CapabilityActor entity.
     *
     * @Route("/{id}/edit", name="capabilityactor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:CapabilityActor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CapabilityActor entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CapabilityActor entity.
    *
    * @param CapabilityActor $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CapabilityActor $entity)
    {
        $form = $this->createForm(new CapabilityActorType(), $entity, array(
            'action' => $this->generateUrl('capabilityactor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CapabilityActor entity.
     *
     * @Route("/{id}", name="capabilityactor_update")
     * @Method("PUT")
     * @Template("AnonymousCobraBundle:CapabilityActor:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:CapabilityActor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CapabilityActor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('capabilityactor_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a CapabilityActor entity.
     *
     * @Route("/{id}", name="capabilityactor_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnonymousCobraBundle:CapabilityActor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CapabilityActor entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('capabilityactor'));
    }

    /**
     * Creates a form to delete a CapabilityActor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('capabilityactor_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
