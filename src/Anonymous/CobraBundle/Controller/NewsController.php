<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Dashboard controller.

     * @Route("/", name="news")
 
 */
class NewsController extends Controller {

    /**
     * Lists all Place entities.
     *
     * @Route("/news", name="news")
     * @Method("GET")
     * @Template("AnonymousCobraBundle:News:index.html.twig")
     */
    public function indexAction() {
  
 
        return $this->render('AnonymousCobraBundle:News:index.html.twig');
    }

}
