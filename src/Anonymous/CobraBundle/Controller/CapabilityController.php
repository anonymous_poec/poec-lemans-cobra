<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Anonymous\CobraBundle\Entity\Capability;
use Anonymous\CobraBundle\Form\CapabilityType;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Capability controller.
 *
 * @Route("/capability")
 */
class CapabilityController extends FOSRestController
{

    /**
     * Lists all Capability entities.
     *
     * @Route("/", name="capability")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AnonymousCobraBundle:Capability')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Capability entity.
     *
     * @Route("/", name="capability_create")
     * @Method("POST")
     * @Template("AnonymousCobraBundle:Capability:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Capability();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('capability_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Capability entity.
     *
     * @param Capability $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Capability $entity)
    {
        $form = $this->createForm(new CapabilityType(), $entity, array(
            'action' => $this->generateUrl('capability_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Capability entity.
     *
     * @Route("/new", name="capability_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Capability();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Capability entity.
     *
     * @Route("/{id}", name="capability_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Capability')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Capability entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Capability entity.
     *
     * @Route("/{id}/edit", name="capability_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Capability')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Capability entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Capability entity.
    *
    * @param Capability $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Capability $entity)
    {
        $form = $this->createForm(new CapabilityType(), $entity, array(
            'action' => $this->generateUrl('capability_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Capability entity.
     *
     * @Route("/{id}", name="capability_update")
     * @Method("PUT")
     * @Template("AnonymousCobraBundle:Capability:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Capability')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Capability entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('capability_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Capability entity.
     *
     * @Route("/{id}", name="capability_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnonymousCobraBundle:Capability')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Capability entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('capability'));
    }

    /**
     * Creates a form to delete a Capability entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('capability_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
