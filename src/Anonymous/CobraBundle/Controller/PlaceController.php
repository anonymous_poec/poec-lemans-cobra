<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Anonymous\CobraBundle\Entity\Place;
use Anonymous\CobraBundle\Form\PlaceType;
use FOS\RestBundle\Controller\FOSRestController;
use \Anonymous\CobraBundle\Entity\PlaceUser;

/**
 * Place controller.
 *
 * @Route("/place")
 */
class PlaceController extends FOSRestController {

    /**
     * Lists all Place entities.
     *
     * @Route("/", name="place")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser()->getId();


        $entities = $em->getRepository('AnonymousCobraBundle:Place')->findPlaceByUser($user);

        $nbZones = array();
        foreach ($entities as $zone) {

            $nbZones[$zone->getId()] = $zone->getZone($em->getRepository('AnonymousCobraBundle:Zone')->countzone($zone->getId()));
        }


        if (!$entities) {
            return $this->redirect($this->generateUrl('place_new'));
        }


        $deleteForms = array();
        Foreach ($entities as $place) {

            $deleteForms[$place->getId()] = $this->createDeleteForm($place->getId())->createView();
        }

        $entitiesCity = $em->getRepository('AnonymousCobraBundle:City')->findAll();
        return array(
            'entities' => $entities,
            'entitiesCity' => $entitiesCity,
            'deleteForms' => $deleteForms,
            'result' => $nbZones,
            'deleteForms' => $deleteForms,
        );
    }

    /**
     * Creates a new Place entity.
     *
     * @Route("/", name="place_create")
     * @Method("POST")
     * @Template("AnonymousCobraBundle:Place:new.html.twig")
     */
    public function createAction(Request $request) {
        $place = new Place();
        $form = $this->createCreateForm($place);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $place->setPerson($this->container->get('security.context')->getToken()->getUser());

            $em->persist($place);
            $em->flush();
            addUserPlace($place);
            return $this->redirect($this->generateUrl('place_show', array('id' => $place->getId())));
        }

        return array(
            'entity' => $place,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Place entity.
     *
     * @param Place $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Place $entity) {
        $form = $this->createForm(new PlaceType(), $entity, array(
            'action' => $this->generateUrl('place_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Place entity.
     *
     * @Route("/new", name="place_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Place();
        $form = $this->createCreateForm($entity);


        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Place entity.
     *
     * @Route("/{id}", name="place_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Place')->find($id);
        $user = $this->get('security.token_storage')->getToken()->getUser()->getId();

        $placeAvailable = $em->getRepository('AnonymousCobraBundle:Person')->getUserFromPlace($entity);
        $istrue = false;
        foreach ($placeAvailable as $place) {
            if ($user == $place->getId()) {
                $istrue = true;
            }
        }
        if (!$entity || $istrue == false) {
            throw $this->createNotFoundException('Unable to find Place entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Place entity.
     *
     * @Route("/{id}/edit", name="place_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Place')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Place entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Place entity.
     *
     * @param Place $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Place $entity) {
        $form = $this->createForm(new PlaceType(), $entity, array(
            'action' => $this->generateUrl('place_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Place entity.
     *
     * @Route("/{id}", name="place_update")
     * @Method("PUT")
     * @Template("AnonymousCobraBundle:Place:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AnonymousCobraBundle:Place')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Place entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('place_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Place entity.
     *
     * @Route("/{id}", name="place_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AnonymousCobraBundle:Place')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Place entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('place'));
    }

    /**
     * Creates a form to delete a Place entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('place_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete',
                            'attr' => array(
                                'onclick' => 'return confirm("Are you sure?")'
                    )))
                        ->getForm()
        ;
    }

    public function addUserPlace($place) {
        $em = $this->getDoctrine()->getManager();
        $placeUser = new PlaceUser();
        $placeUser->setUser($this->container->get('security.context')->getToken()->getUser());
        $placeUser->setPlace($place);
        $em->persist($placeUser);
        $em->flush();
    }

}
