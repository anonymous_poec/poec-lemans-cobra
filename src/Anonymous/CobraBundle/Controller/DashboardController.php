<?php

namespace Anonymous\CobraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Dashboard controller.
 *
 * @Route("/dashboard")
 */
class DashboardController extends Controller {

    /**
     * Lists all Place entities.
     *
     * @Route("/", name="dashboard")
     * @Method("GET")
     * @Template("AnonymousCobraBundle:Dashboard:index.html.twig")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser()->getId();

        $getOnePlace = $em->getRepository('AnonymousCobraBundle:Place')->findOnePlace($user);
        $getListUser = $em->getRepository('AnonymousCobraBundle:Person')->getUserFromPlace($getOnePlace);
        
 
        return $this->render('AnonymousCobraBundle:Dashboard:index.html.twig',array('users'=>$getListUser));
    }

}
