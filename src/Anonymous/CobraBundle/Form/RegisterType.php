<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegisterType extends BaseType {

    private $class;

    public function __construct($class) {
        parent::__construct($class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
                ->add('firstname', null)
                ->add('lastname', null)
                
        ;
        parent::buildForm($builder,$options);
    }

    public function getName() {
        return 'Register';
    }

}
