<?php

namespace Anonymous\CobraBundle\Repository;

/**
 * ZoneRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ZoneRepository extends \Doctrine\ORM\EntityRepository {

    public function countzone($id) {
        $qb = $this->createQueryBuilder('z')
                ->select('count(z.id)')
                ->where('z.place = :place')
                ->setParameter('place', $id)
               ;
        $query = $qb->getQuery()->getResult();
    
        
        return $query;
    }

}
