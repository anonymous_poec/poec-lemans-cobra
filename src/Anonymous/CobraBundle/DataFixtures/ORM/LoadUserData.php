<?php

namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Person;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {


        // Create a new user
        $user = new Person();
        $user->setUsername('123');
        $user->setEmail('user@domain.com');
        $user->setPlainPassword('123');
        $user->setEnabled(true);
        $user->setRoles(array(Person::ROLE_SUPER_ADMIN,));


        $manager->persist($user);
        $manager->flush();
        $this->addReference('user1', $user);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder() {
        return 100;
    }

}
