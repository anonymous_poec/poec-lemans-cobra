<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadPlaceData.php

namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Place;

class LoadPlaceData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $em) {
        $user = $em->getRepository('AnonymousCobraBundle:Person')->findAll();
        foreach ($user as $u) {
            $id = $u->getId();
        }
        $user = $em->getRepository('AnonymousCobraBundle:Person')->findOneBy(array('id' => $id));
        $place1 = new Place();
        $place1->setName('Residence Principale');
        $place1->setAdress('4 impasse Leclerc');
        $place1->setCity($em->merge($this->getReference('city1')));
        $place1->setOwner(true);
        // $place1->setZone('');
        $place1->setPerson($user);

        $place2 = new Place();
        $place2->setName('Mobile Home');
        $place2->setAdress('10 rue de marseille');
        $place2->setCity($em->merge($this->getReference('city2')));
        $place2->setOwner(false);
        // $place2->setZone('');
        $place2->setPerson($user);
        
        $place3 = new Place();
        $place3->setName('chateau');
        $place3->setAdress('15 rue de toto');
        $place3->setCity($em->merge($this->getReference('city3')));
        $place3->setOwner(false);
        // $place2->setZone('');
        $place3->setPerson($user);
        
        $place4 = new Place();
        $place4->setName('apartement');
        $place4->setAdress('2 rue de titi');
        $place4->setCity($em->merge($this->getReference('city4')));
        $place4->setOwner(false);
        // $place2->setZone('');
        $place4->setPerson($user);



        $em->persist($place1);
        $em->persist($place2);
        $em->persist($place3);
        $em->persist($place4);

        $em->flush();

        $this->addReference('place1', $place1);
        $this->addReference('place2', $place2);
        $this->addReference('place3', $place3);
        $this->addReference('place4', $place4);
    }

    public function getOrder() {
        return 400;
    }

}
