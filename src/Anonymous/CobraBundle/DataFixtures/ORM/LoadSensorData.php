<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadSensorData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Sensor;
 
class LoadSensorData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $sensor1 = new Sensor();
    $sensor1->setName('sensor light parking');
   // $sensor1->addCapabilitySensor($em->merge($this->getReference('capability-sensor1')));
    
    
    $sensor2 = new Sensor();
    $sensor2->setName('sensor light outside');
    //$sensor2->addCapabilitySensor($em->merge($this->getReference('capability-sensor2')));

    $em->persist($sensor1);
    $em->persist($sensor2);
 
    $em->flush();
    $this->addReference('sensor1', $sensor1);
    $this->addReference('sensor2', $sensor2);
  
  }

    public function getOrder() {
        return 900;
    }

 
}