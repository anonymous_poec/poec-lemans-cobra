<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadCapabilitiesData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Capability;
 
class LoadCapabilitiesData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $electric = new Capability();
    $electric->setName('capabilitie electric');
    //$electric->setactor('');
   // $electric->setsensor('');
    // $electric->setnode($this->getReference('node1'));
    
    $light = new Capability();
    $light->setName('capabilitie light ');
    //$light->setactor('');
    //$light->setsensor('');
     $light->setnode($em->merge($this->getReference('node1')));
   
    $heating = new Capability();
    $heating->setName('capabilitie heating ');
    //$heating->setactor('');
    //$heating->setsensor('');
    // $electric->setnode('');
    
    $store = new Capability();
    $store->setName('capabilitie store ');
    //$store->setactor('');
    //$store->setsensor('');
    // $electric->setnode('');
    
    $weather = new Capability();
    $weather->setName('capabilitie weather ');
   // $weather->setactor('');
   // $weather->setsensor('');
    // $electric->setnode('');
    
   
    $em->persist($electric);
    $em->persist($light);
    $em->persist($heating);
    $em->persist($store);
    $em->persist($weather);
 
    $em->flush();
    
    $this->addReference('capabilitie-electric', $electric);
    $this->addReference('capabilitie-light', $light);
    $this->addReference('capabilitie-heating', $heating);
    $this->addReference('capabilitie-store', $store);
    $this->addReference('capabilitie-weather', $weather);
    
  }

    public function getOrder() {
        return 700;
    }

 
}