<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadCityData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\City;
 
class LoadCityData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $city1 = new City();
    $city1->setCity('le mans');
    $city1->setZip('72000');
   // $city1->setPlace($em->merge($this->getReference('place1')));
    
    $city2 = new City();
    $city2->setCity('paris');
    $city2->setZip('75000');
  //  $city2->setPlace($em->merge($this->getReference('place2')));
    
    $city3 = new City();
    $city3->setCity('rennes');
    $city3->setZip('35000');
    //$city3->setPlace('');
    
    $city4 = new City();
    $city4->setCity('marseille');
    $city4->setZip('13000');
    //$city4->setPlace('');*/

    $em->persist($city1);
    $em->persist($city2);
    $em->persist($city3);
    $em->persist($city4);
    
    $em->flush();
    
    $this->addReference('city1', $city1);
    $this->addReference('city2', $city2);
    $this->addReference('city3', $city3);
    $this->addReference('city4', $city4);
    //$this->addReference('city3', $city3);
   // $this->addReference('city4', $city4);
  }

    public function getOrder() {
        return 300;
    }

 
}