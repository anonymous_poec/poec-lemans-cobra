<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadZoneData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Zone;
 
class LoadZoneData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $zone1 = new Zone();
    $zone1->setName('living room');
    $zone1->setPlace($em->merge($this->getReference('place1')));
    $zone1->setIsInDoor(false);
    //$zone1->setNode('5');
    
    
    $zone2 = new Zone();
    $zone2->setName('bed room');
    $zone2->setPlace($em->merge($this->getReference('place2')));
    $zone2->setIsInDoor(false);
    //$zone2->setNode('2');

    $em->persist($zone1);
    $em->persist($zone2);
 
    $em->flush();
    
    $this->addReference('zone1', $zone1);
    $this->addReference('zone2', $zone2);
  }

    public function getOrder() {
        return 500;
    }

 
}