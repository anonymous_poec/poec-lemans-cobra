<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadActorData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Actor;
 
class LoadActorData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $actor1 = new Actor();
    $actor1->setName('actor light parking');
    //$actor1->addCapabilityActor($em->merge($this->getReference('capability-actor1')));
    
    $actor2 = new Actor();
    $actor2->setname('actor light outside');
    //$actor2->addCapabilityActor($em->merge($this->getReference('capability-actor2')));       
   

    $em->persist($actor1);
    $em->persist($actor2);
 
    $em->flush();
    
    $this->addReference('actor1', $actor1);
    $this->addReference('actor2', $actor2);
  }

    public function getOrder() {
        return 800;
    }

        
}