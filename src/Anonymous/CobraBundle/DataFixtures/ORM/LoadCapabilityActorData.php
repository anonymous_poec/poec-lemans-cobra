<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadCapabilitiesActorData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\CapabilityActor;
 
class LoadCapabilitiesActorData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $CapabilityActor1 = new CapabilityActor();
    $CapabilityActor1->setActor($this->getReference('actor1'));
    $CapabilityActor1->setCapability($this->getReference('capabilitie-light'));
    
    $CapabilityActor2 = new CapabilityActor();
    $CapabilityActor2->setActor($em->merge($this->getReference('actor2')));
    $CapabilityActor2->setCapability($this->getReference('capabilitie-light'));
    
   
    $em->persist($CapabilityActor1);
    $em->persist($CapabilityActor2);
    
    $em->flush();
    
    $this->addReference('capability-actor1', $CapabilityActor1);
    $this->addReference('capability-actor2', $CapabilityActor2);
    
    
  }

    public function getOrder() {
        return 1000;
    }

    

}