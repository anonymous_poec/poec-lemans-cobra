<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadCapabilitiesData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\CapabilitySensor;
 
class LoadCapabilitiesSensorData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $CapabilitySensor1 = new CapabilitySensor();
    $CapabilitySensor1->setSensor($this->getReference('sensor1'));
    $CapabilitySensor1->setCapability($this->getReference('capabilitie-light'));
    
    $CapabilitySensor2 = new CapabilitySensor();
    $CapabilitySensor2->setSensor($em->merge($this->getReference('sensor2')));
    $CapabilitySensor2->setCapability($this->getReference('capabilitie-light'));
    
   
    $em->persist($CapabilitySensor1);
    $em->persist($CapabilitySensor2);
    
    $em->flush();
    
    $this->addReference('capability-sensor1', $CapabilitySensor1);
    $this->addReference('capability-sensor2', $CapabilitySensor2);
    
    
  }

    public function getOrder() {
        return 1100;
    }

    

}