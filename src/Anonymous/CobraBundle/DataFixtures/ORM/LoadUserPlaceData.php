<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\PlaceUser;

class LoadUserPlaceData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $em) {
        $placeUser = new PlaceUser();
        $placeUser->setPlace($em->merge($this->getReference("place1")));
        $placeUser->setUser($em->merge($this->getReference("user1")));

        $placeUser1 = new PlaceUser();
        $placeUser1->setPlace($em->merge($this->getReference("place2")));
        $placeUser1->setUser($em->merge($this->getReference("user1")));

        $em->persist($placeUser);
        $em->persist($placeUser1);

        $em->flush();

        $this->addReference('placeUser', $placeUser);
        $this->addReference('placeUser1', $placeUser1);
    }

    public function getOrder() {
        return 1200;
    }

}
