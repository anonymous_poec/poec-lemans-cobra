<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadNodeData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Node;
 
class LoadNodeData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $node1 = new Node();
    $node1->setName('light Parking');
    //$node1->addCapability($this->getReference('capabilitie-light'));
    $node1->setIpAdress('192.168.1.100');
    $node1->setSecurity('10');
    $node1->setZone($this->getReference('zone1'));
    
    
    $node2 = new Node();
    $node2->setName('light Outside');
    //$node2->addCapability($em->merge($this->getReference('capabilitie-light')));
    $node2->setIpAdress('192.168.1.101');
    $node2->setSecurity('10');
    $node2->setZone($this->getReference('zone1'));
    
   
    $em->persist($node1);
    $em->persist($node2);
 
    $em->flush();
    
    $this->addReference('node1', $node1);
    $this->addReference('node2', $node2);
    
  }

    public function getOrder() {
        return 600;
    }

 
}