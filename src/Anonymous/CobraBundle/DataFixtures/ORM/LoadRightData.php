<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Anonymous/CobraBundle/DataFixtures/ORM/LoadRightsData.php
namespace Anonymous\CobraBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Anonymous\CobraBundle\Entity\Right;
 
 class LoadRightData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
        $user = $em->getRepository('AnonymousCobraBundle:Person')->findAll();
        foreach ($user as $u) {
            $id = $u->getId();
        }
        $user = $em->getRepository('AnonymousCobraBundle:Person')->findOneBy(array('id' => $id));
    $owner = new Right();
    $owner->setName('owner');
    $owner->setDescription('1');
    $owner->setPerson($user);
  
   
    $em->persist($owner);

 
    $em->flush();
    
    $this->addReference('owner', $owner);
  }

    public function getOrder() {
        return 200;
    }

 
}